#!/usr/bin/env python3
#
########################################################################
# Copyright (C) 2020 Mark J. Blair, NF6X
#
# This file is part of papertape
#
#  papertape is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  papertape is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with py  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""Example code to create a bunch of 8 bit test pattern tapes."""

import papertape

# All 0x00
tape = papertape.tape(bytearray(256))
tape.add_leader(60)
tape.add_trailer(60)
tape.add_title('00')
tape.add_leader(20)
tape.save('test8_00.bin')

# All 0xFF
tape = papertape.tape(bytearray([0xFF]*256))
tape.add_leader(60)
tape.add_trailer(60)
tape.add_title('FF')
tape.add_leader(20)
tape.save('test8_FF.bin')

# Alternating 0xFF 0x00
tape = papertape.tape(bytearray([0xFF, 0x00]*128))
tape.add_leader(60)
tape.add_trailer(60)
tape.add_title('FF 00')
tape.add_leader(20)
tape.save('test8_FF00.bin')

# Alternating 0xAA 0x55
tape = papertape.tape(bytearray([0xAA, 0x55]*128))
tape.add_leader(60)
tape.add_trailer(60)
tape.add_title('AA 55')
tape.add_leader(20)
tape.save('test8_AA55.bin')

# 0x00 through 0xFF
tape = papertape.tape(bytearray([n for n in range(256)]))
tape.add_leader(60)
tape.add_trailer(60)
tape.add_title('00-FF')
tape.add_leader(20)
tape.save('test8_00-FF.bin')

# Walking 1
tape = papertape.tape(bytearray(([0x01, 0x02, 0x04, 0x08,
                                  0x10, 0x20, 0x40, 0x80,
                                  0x40, 0x20, 0x10, 0x08,
                                  0x04, 0x02]*17) + [0x01]))
tape.add_leader(60)
tape.add_trailer(60)
tape.add_title('WALKING 1')
tape.add_leader(20)
tape.save('test8_walking1.bin')

# Printable ASCII, no parity
tape = papertape.tape(bytearray([n for n in range(0x20, 0x7F, 1)]))
tape.add_leader(60)
tape.add_trailer(60)
tape.add_title('ASCII NONE')
tape.add_leader(20)
tape.save('test8_ascii-none.bin')

# Printable ASCII, odd parity
tape = papertape.tape(bytearray([n for n in range(0x20, 0x7F, 1)]))
tape.odd_parity()
tape.add_leader(60)
tape.add_trailer(60)
tape.add_title('ASCII ODD')
tape.add_leader(20)
tape.save('test8_ascii-odd.bin')

# Printable ASCII, even parity
tape = papertape.tape(bytearray([n for n in range(0x20, 0x7F, 1)]))
tape.even_parity()
tape.add_leader(60)
tape.add_trailer(60)
tape.add_title('ASCII EVEN')
tape.add_leader(20)
tape.save('test8_ascii-even.bin')

# Printable ASCII, mark parity
tape = papertape.tape(bytearray([n for n in range(0x20, 0x7F, 1)]))
tape.set_msb()
tape.add_leader(60)
tape.add_trailer(60)
tape.add_title('ASCII MARK')
tape.add_leader(20)
tape.save('test8_ascii-mark.bin')

